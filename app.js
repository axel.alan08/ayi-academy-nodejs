require("dotenv").config();
const express = require("express");
const morgan = require("morgan");

const shoppingRoutes = require("./routes/shoppingRoutes");
const userRoutes = require("./routes/userRoutes");
const commerceRoutes = require("./routes/commerceRoutes");

const app = express();

app.use(morgan('tiny'));

app.use(express.json());
app.use(express.text());

app.use("/commerce", commerceRoutes);
app.use("/shopping", shoppingRoutes);
app.use("/user", userRoutes);

app.listen(process.env.PORT, () => {
  console.log(`Servidor corriendo en puerto ${process.env.PORT}`);
});