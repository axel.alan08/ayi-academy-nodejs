const getCommerce = (req, res) => {
    res.status(200).send('Ok');
};

const postCommerce = (req, res) => {
    res.status(200).send('Ok');
};

const putCommerce = (req, res) => {
    res.status(200).send('Ok');
};

const deleteCommerce = (req, res) => {
    res.status(200).send('Ok')
};

module.exports = {
    getCommerce,
    postCommerce,
    putCommerce,
    deleteCommerce,
};