const getShopping = (req, res) => {
    res.status(200).send('Ok');
};

const postShopping = (req, res) => {
    res.status(200).send('Ok');
};

const putShopping = (req, res) => {
    res.status(200).send('Ok');
};

const deleteShopping = (req, res) => {
    res.status(200).send('Ok')
};

module.exports = {
    getShopping,
    postShopping,
    putShopping,
    deleteShopping,
};