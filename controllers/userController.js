const getUser = (req, res) => {
    res.status(200).send('Ok');
};

const postUser = (req, res) => {
    res.status(200).send('Ok');
};

const putUser = (req, res) => {
    res.status(200).send('Ok');
};

const deleteUser = (req, res) => {
    res.status(200).send('Ok')
};

module.exports = {
    getUser,
    postUser,
    putUser,
    deleteUser,
};