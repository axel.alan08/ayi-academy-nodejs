const Joi = require("joi");

const validateIdParamUser = async (req, res, next) => {
  const schema = Joi.object({
    _id: Joi.string().required(),
  }).required();  

try {
  await schema.validateAsync(req.params);
  return next();
} catch (err) {
  console.log(err);
  return res.status(400).send({
      mensaje: "Datos de entrada no válidos"
   })
  }
};

const validatePostUser = async (req, res, next) => {
  const schema = Joi.object({
    _id: Joi.string().required(),
    nombre: Joi.string().required(),
    apellido: Joi.string().required(),
    fec_nac: Joi.date().required(),
    password: Joi.string().required(),
    email: Joi.string().required(),
    roles: Joi.array().items(
        Joi.string().required().valid(
          "ADMIN",
          "SHOPPING_MANAGER",
          "COMMERCE_MANAGER",
          "USER"
        )
      )
      .min(4)
      .max(4)
  });
 
  try {
    await schema.validateAsync(req.body);
    return next();
  } catch (err) {
    console.log(err);
    return res.status(400).send({
        mensaje: "Datos de entrada no válidos"
    })
  }
};

const validatePutUser = async (req, res, next) => {
  const schema = Joi.object({
    password: Joi.string(),
    email: Joi.string().email(),
    roles: Joi.array().items(
      Joi.string().valid(
        "ADMIN",
        "SHOPPING_MANAGER",
        "COMMERCE_MANAGER",
        "USER"
    )
    )
      .max(4),
  })

  try {
    await schema.validateAsync(req.body);
    return next();
  } catch (err) {
    console.log(err);
    return res.status(400).send({
        mensaje: "Datos de entrada no válidos"
    })
  }
};


module.exports = {
  validatePutUser,
  validatePostUser,
  validateIdParamUser,
};
