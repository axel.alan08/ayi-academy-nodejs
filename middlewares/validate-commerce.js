const Joi = require("joi");

const validateIdParamCommerce = async (req, res, next) => {
  const schema = Joi.object({
    _id: Joi.string().required(),
  }).required();  

try {
  await schema.validateAsync(req.params);
  return next();
} catch (err) {
  console.log(err);
  return res.status(400).send({
      mensaje: "Datos de entrada no válidos"
   })
  }
};

const validatePostCommerce = async (req, res, next) => {
  const schema = Joi.object({
    _id: Joi.string().required(),
    nombre: Joi.string().required(),
    manager: Joi.string().required(),
    shopping: Joi.string().required(),
  });

 
  try {
    await schema.validateAsync(req.body);
    return next();
  } catch (err) {
    console.log(err);
    return res.status(400).send({
        mensaje: "Datos de entrada no válidos"
    })
  }
};

const validatePutCommerce = async (req, res, next) => {
  const schema = Joi.object({
    manager: Joi.string(),
    shopping: Joi.string(),
  })

  try {
    await schema.validateAsync(req.body);
    return next();
  } catch (err) {
    console.log(err);
    return res.status(400).send({
      mensaje: "Datos de entrada no válidos"
    })
  }
};


module.exports = {
  validatePutCommerce,
  validatePostCommerce,
  validateIdParamCommerce,
};