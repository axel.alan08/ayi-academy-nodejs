const Joi = require("joi");

const validateIdParamShopping = async (req, res, next) => {
  const schema = Joi.object({
    _id: Joi.string().required(),
  }).required();  

try {
  await schema.validateAsync(req.params);
  return next();
} catch (err) {
  console.log(err);
  return res.status(400).send({
      mensaje: "Datos de entrada no válidos"
   })
  }
};

const validatePostShopping = async (req, res, next) => {
  const schema = Joi.object({
    _id: Joi.string().required(),
    nombre: Joi.string().required(),
    manager: Joi.string().required(),
    comercios: Joi.array().items(
      Joi.string()
    )
    .required(),
  });

 
  try {
    await schema.validateAsync(req.body);
    return next();
  } catch (err) {
    console.log(err);
    return res.status(400).send({
        mensaje: "Datos de entrada no válidos"
    })
  }
};

const validatePutShopping = async (req, res, next) => {
  const schema = Joi.object({
    manager: Joi.string(),
    comercios: Joi.array().items(
      Joi.string()
    ),
  })
  
  try {
    await schema.validateAsync(req.body);
    return next();
  } catch (err) {
    console.log(err);
    return res.status(400).send({
        mensaje: "Datos de entrada no válidos"
    })
  }
};


module.exports = {
  validatePutShopping,
  validatePostShopping,
  validateIdParamShopping,
};