const express = require('express');
const router = express.Router();

const {
    getUser,
    postUser,
    putUser,
    deleteUser,
} = require ("../controllers/userController");

const {
    validateIdParamUser,
    validatePostUser,
    validatePutUser,
} = require("../middlewares/validate-user");

router.get('/:_id', validateIdParamUser, getUser);
router.post('/', validatePostUser, postUser);
router.put('/:_id', validatePutUser, validateIdParamUser, putUser);
router.delete('/:_id', validateIdParamUser, deleteUser);

module.exports = router;