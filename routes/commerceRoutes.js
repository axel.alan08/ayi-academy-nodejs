const express = require('express');
const router = express.Router();

const {
    getCommerce,
    postCommerce,
    putCommerce,
    deleteCommerce,
} = require ("../controllers/commerceController");

const {
    validateIdParamCommerce,
    validatePostCommerce,
    validatePutCommerce,
} = require("../middlewares/validate-commerce");

router.get('/:_id', validateIdParamCommerce, getCommerce);
router.post('/', validatePostCommerce, postCommerce);
router.put('/:_id', validatePutCommerce, validateIdParamCommerce, putCommerce);
router.delete('/:_id', validateIdParamCommerce, deleteCommerce);

module.exports = router;