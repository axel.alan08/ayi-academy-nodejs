const express = require('express');
const router = express.Router();

const {
    getShopping,
    postShopping,
    putShopping,
    deleteShopping,
} = require ("../controllers/shoppingController");

const {
    validateIdParamShopping,
    validatePostShopping,
    validatePutShopping,
} = require("../middlewares/validate-shopping");

router.get('/:_id', validateIdParamShopping, getShopping);
router.post('/', validatePostShopping, postShopping);
router.put('/:_id', validatePutShopping, validateIdParamShopping, putShopping);
router.delete('/:_id', validateIdParamShopping, deleteShopping);

module.exports = router;